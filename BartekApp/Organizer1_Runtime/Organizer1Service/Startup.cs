using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Organizer1Service.Startup))]

namespace Organizer1Service
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}