﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBOrganizer;
using Microsoft.WindowsAzure.MobileServices;
using Organizer1.Models;

namespace Organizer1.Managers
{
    public partial class ExpensesManager
    {
        static ExpensesManager defaultInstance = new ExpensesManager();
        MobileServiceClient client;

#if OFFLINE_SYNC_ENABLED
        IMobileServiceSyncTable<Expense> _expenseTable;
#else
        readonly IMobileServiceTable<Expense> _expenseTable;
#endif

        const string offlineDbPath = @"localstore.db";

        private ExpensesManager()
        {
            this.client = new MobileServiceClient(Constants.ApplicationURL);

#if OFFLINE_SYNC_ENABLED
            var store = new MobileServiceSQLiteStore(offlineDbPath);
            store.DefineTable<Expense>();

            //Initializes the SyncContext using the default IMobileServiceSyncHandler.
            this.client.SyncContext.InitializeAsync(store);

            this._expenseTable = client.GetSyncTable<Expense>();
#else
            this._expenseTable = client.GetTable<Expense>();
#endif
        }

        public static ExpensesManager DefaultManager
        {
            get
            {
                return defaultInstance;
            }
            private set
            {
                defaultInstance = value;
            }
        }

        public MobileServiceClient CurrentClient => client;  // get return client;

        public bool IsOfflineEnabled
        {
            get { return _expenseTable is Microsoft.WindowsAzure.MobileServices.Sync.IMobileServiceSyncTable<Expense>; }
        }

        public async Task<ObservableCollection<Expense>> GetExpenseItemsAsync(string sortType, bool syncItems = false)
        {
            try
            {
#if OFFLINE_SYNC_ENABLED
                if (syncItems)
                {
                    await this.SyncAsync();
                }
#endif
                IEnumerable<Expense> items;
                if (sortType == "Wszystkie")
                {
                    items = await _expenseTable
                     .Where(expense => !expense.Done)
                     .ToEnumerableAsync();
                }
                else
                {
                    items = await _expenseTable
                    .Where(expense => !expense.Done && expense.Type == sortType)
                    .ToEnumerableAsync();
                }

                return new ObservableCollection<Expense>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }


        public async Task<List<Expense>> GetThisMonthExpenses()
        {
            var thisMonth = DateTime.Now;

            try
            {
#if OFFLINE_SYNC_ENABLED
                if (syncItems)
                {
                    await this.SyncAsync();
                }
#endif
                IEnumerable<Expense> items;
                items = await _expenseTable
                    .Where(expense => !expense.Done && expense.AddDateTime.Month == thisMonth.Month && expense.AddDateTime.Year == thisMonth.Year)
                    .ToListAsync();
                
                return new List<Expense>(items);
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception e)
            {
                Debug.WriteLine(@"Sync error: {0}", e.Message);
            }
            return null;
        }

        public async Task SaveTaskAsync(Expense item)
        {
            if (item.Id == null)
            {
                await _expenseTable.InsertAsync(item);
            }
            else
            {
                await _expenseTable.UpdateAsync(item);
            }
        }

#if OFFLINE_SYNC_ENABLED
        public async Task SyncAsync()
        {
            ReadOnlyCollection<MobileServiceTableOperationError> syncErrors = null;

            try
            {
                await this.client.SyncContext.PushAsync();

                await this._expenseTable.PullAsync(
                    //The first parameter is a query name that is used internally by the client SDK to implement incremental sync.
                    //Use a different query name for each unique query in your program
                    "allExpenseItems",
                    this._expenseTable.CreateQuery());
            }
            catch (MobileServicePushFailedException exc)
            {
                if (exc.PushResult != null)
                {
                    syncErrors = exc.PushResult.Errors;
                }
            }

            // Simple error/conflict handling. A real application would handle the various errors like network conditions,
            // server conflicts and others via the IMobileServiceSyncHandler.
            if (syncErrors != null)
            {
                foreach (var error in syncErrors)
                {
                    if (error.OperationKind == MobileServiceTableOperationKind.Update && error.Result != null)
                    {
                        //Update failed, reverting to server's copy.
                        await error.CancelAndUpdateItemAsync(error.Result);
                    }
                    else
                    {
                        // Discard local change.
                        await error.CancelAndDiscardItemAsync();
                    }

                    Debug.WriteLine(@"Error executing sync operation. Item: {0} ({1}). Operation discarded.", error.TableName, error.Item["id"]);
                }
            }
        }
#endif
    }
}