﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.LocalNotifications;

namespace Organizer1
{
    public class NotificationController
    {

        public static void SendScheduledNotification(string title, string body, int id, DateTime dateTime)
        {
            CrossLocalNotifications.Current.Show(title, body, id, dateTime);
        }
        
    }
}
