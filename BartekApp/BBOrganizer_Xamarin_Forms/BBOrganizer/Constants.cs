﻿using System;

namespace BBOrganizer
{
	public static class Constants
	{
		// Replace strings with your Azure Mobile App endpoint.
		public static string ApplicationURL = @"https://prebborganizer.azurewebsites.net";
        //ENUMS

        //NOTES
        public enum NoteType { Pomysly, Wazne, Luzne }
        public enum SortNoteType { Wszystkie, Pomysly, Wazne, Luzne }

        //Expenses

        public enum ExpensesType { Jedzenie, Samochod, Pierdoly }
        public enum SortExpensesType { Wszystkie, Jedzenie, Samochod, Pierdoly }

        //Kitepol
        //public enum TypeOfKitepolLesson { Kitesurfing,Windsurfing}
	    public static int KitesurfingHourlyRate = 50;
	    public static int WindsurfingHourlyRate = 35;
	}
}

