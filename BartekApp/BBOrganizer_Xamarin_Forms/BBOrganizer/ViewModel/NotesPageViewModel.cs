﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBOrganizer;

namespace Organizer1.ViewModel
{
    public class NotesPageViewModel
    {
        public List<string> NoteTypesList { get; } = Enum.GetNames(typeof(Constants.NoteType)).ToList();

        private int selectedNoteType;

        public List<string> SortNoteTypesList { get; } = Enum.GetNames(typeof(Constants.SortNoteType)).ToList();
        public int SelectedSortNoteType = 0 ;

        //public int SelectedSortNoteType
        //{
        //    get { return NoteTypesList[selectedSortNoteType]; }
        //    set { selectedNoteType = int.Parse(value); }
        //}

        public string SelectedNoteType
        {
            get { return NoteTypesList[selectedNoteType]; }
            set { selectedNoteType = int.Parse(value); }
        }


    }
}
