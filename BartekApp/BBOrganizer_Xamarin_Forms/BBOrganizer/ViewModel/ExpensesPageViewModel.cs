﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBOrganizer;

namespace Organizer1.ViewModel
{
    public class ExpensesPageViewModel
    {
        public List<string> ExpensesTypesList { get; } = Enum.GetNames(typeof(Constants.ExpensesType)).ToList();

        private int _selectedExpenseType;

        public List<string> SortExpensesTypesList { get; } = Enum.GetNames(typeof(Constants.SortExpensesType)).ToList();
        public int SelectedSortExpenseType = 0;

        public string SelectedNoteType
        {
            get { return ExpensesTypesList[_selectedExpenseType]; }
            set { _selectedExpenseType = int.Parse(value); }
        }


    }
}
