﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Organizer1.Models;
using Organizer1.Models.Kitepol;

namespace Organizer1.ViewModel
{
    public class KitepolWorkPageViewModel : INotifyPropertyChanged
    {

        public List<string> SortLessonList { get; } = new List<string>{"Wszystkie","Kitesurfing","Windsurfing"};
        public string TotalKitepolHours { get; set; }
        
        public ObservableCollection<KitepolLesson> NewLessonsEntryList { get; set; }

        public void InitiatieAllLessons(int numberOflessons)
        {
            NewLessonsEntryList = new ObservableCollection<KitepolLesson>();
            for (int i = 0; i < numberOflessons; i++)
            {
                NewLessonsEntryList.Add(new KitepolLesson{Student = "", Hours = 0, Type = "", Date = DateTime.Today});
            }
        }
      

        public event PropertyChangedEventHandler PropertyChanged;
        private void SetChangedProperty(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

    }

}

