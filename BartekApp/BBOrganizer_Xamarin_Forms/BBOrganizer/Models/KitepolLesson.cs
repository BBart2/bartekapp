﻿using System;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.MobileServices;

namespace Organizer1.Models.Kitepol
{
    public class KitepolLesson
    {
        private string _type;

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        
        [JsonProperty(PropertyName = "student")]
        public string Student { get; set; }

        [JsonProperty(PropertyName = "hours")]
        public float Hours { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type {
            get { return _type; }
            set
            {
                switch (value)
                {
                    case "0":
                        _type = "Kitesurfing";
                        break;
                    case "1":
                        _type = "Windsurfing";
                        break;
                    case "Kitesurfing":
                        _type = "Kitesurfing";
                        break;
                    case "Windsurfing":
                        _type = "Windsurfing";
                        break;
                    default:
                        _type = "0";
                        break;
                }
            }
        }

        [JsonProperty(PropertyName = "date")]
        public DateTime Date { get; set; }

        [JsonProperty(PropertyName = "complete")]
        public bool Done { get; set; }

        [Version]
        public string Version { get; set; }

        //display 
        public string DateAdded => Date.Day + " / " + Date.Month;

        public string HoursDisplay => Hours.ToString() + "h";

    }
}