﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.MobileServices;
using Newtonsoft.Json;

namespace Organizer1.Models
{
    public class Expense
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public int Amount { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "AddDateTime")]
        public DateTime AddDateTime { get; set; }

        [JsonProperty(PropertyName = "complete")]
        public bool Done { get; set; }

        [Version]
        public string Version { get; set; }


        //displaying


        public string AmountToDisplay => Amount + " zl";  // return name + amount

        public string DateAdded => AddDateTime.Day + " / " + AddDateTime.Month;


    }
}
