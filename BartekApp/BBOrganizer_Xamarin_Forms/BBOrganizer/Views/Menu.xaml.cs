﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBOrganizer;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Organizer1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Menu : MasterDetailPage
    {
        public Menu()
        {
            InitializeComponent();

            this.Master = new MasterPage();
            this.Detail = new NavigationPage(new DetailsPage());

            App.MasterDetailPage = this;
        }
    }
}
