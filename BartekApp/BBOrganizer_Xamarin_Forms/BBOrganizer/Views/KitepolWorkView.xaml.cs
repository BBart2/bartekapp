﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBOrganizer;
using Organizer1.Managers;
using Organizer1.Models;
using Organizer1.Models.Kitepol;
using Organizer1.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Organizer1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class KitepolWorkView : ContentPage
    {
        private readonly KitepolWorkManager _manager;
        private readonly KitepolLessonManager _lessonManager;

        private static KitepolWorkPageViewModel _kitePolWorkPageViewModel;
        public bool ShowNewLessonsGrid = false;


        public KitepolWorkView()
        {
            InitializeComponent();

            this.BindingContext = _kitePolWorkPageViewModel = new KitepolWorkPageViewModel();

            _manager = KitepolWorkManager.DefaultManager;
            _lessonManager = KitepolLessonManager.DefaultManager;;

            ViewInitialization();

            // OnPlatform<T> doesn't currently support the "Windows" target platform, so we have this check here.
            if (_manager.IsOfflineEnabled &&
                (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.WinPhone))
            {
                var syncButton = new Button
                {
                    Text = "Sync items",
                    HeightRequest = 30
                };
                syncButton.Clicked += OnSyncItems;

                ButtonsPanel.Children.Add(syncButton);
            }
        }


        private class ActivityIndicatorScope : IDisposable
        {
            private bool _showIndicator;
            private ActivityIndicator _indicator;
            private Task _indicatorDelay;


            public ActivityIndicatorScope(ActivityIndicator indicator, bool showIndicator)
            {
                this._indicator = indicator;
                this._showIndicator = showIndicator;

                if (showIndicator)
                {
                    _indicatorDelay = Task.Delay(2000);
                    SetIndicatorActivity(true);
                }
                else
                {
                    _indicatorDelay = Task.FromResult(0);
                }
            }

            private void SetIndicatorActivity(bool isActive)
            {
                this._indicator.IsVisible = isActive;
                this._indicator.IsRunning = isActive;
            }

            public void Dispose()
            {
                if (_showIndicator)
                {
                    _indicatorDelay.ContinueWith(t => SetIndicatorActivity(false),
                        TaskScheduler.FromCurrentSynchronizationContext());
                }
            }
        }

        private void ViewInitialization()
        {
            KitepolSortTypePicker.SelectedIndex = 0;

            //KitepolSortTypePicker.SelectedIndexChanged += OnRefresh;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            // Set syncItems to true in order to synchronize the data on startup when running in offline mode
            await RefreshItems(true, syncItems: true);
        }
        
        //DataMethods
        async Task AddLessons(List<KitepolLesson> lessons)
        {
            await _lessonManager.SaveTaskAsync(lessons);
            await RefreshItems(false, true);
        }

        async Task CompleteItem(KitepolLesson item)
        {
            item.Done = true;
           // await _lessonManager.SaveTaskAsync(item);
            //ExpensesList.ItemsSource = await _manager.GetExpenseItemsAsync("Wszystkie");
            //await UpdateMonthExpenses();
        }

        public async void OnAdd(object sender, EventArgs e)
        {
            await AddLessons(_kitePolWorkPageViewModel.NewLessonsEntryList.ToList());
            
            ShowOrHideNewLessonsPanel(null, null);
        }

        // Event handlers
        public async void OnSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var kitepolLesson = e.SelectedItem as KitepolLesson;
            if (Device.OS != TargetPlatform.iOS && kitepolLesson != null)
            {
                // Not iOS - the swipe-to-delete is discoverable there
                if (Device.OS == TargetPlatform.Android)
                {
                    await DisplayAlert(kitepolLesson.Date.ToString(), "Press-and-hold to complete task " + kitepolLesson.Date.ToString(), "Got it!");
                }
                else
                {
                    // Windows, not all platforms support the Context Actions yet
                    if (await DisplayAlert("Mark completed?", "Do you wish to complete " + kitepolLesson.Date.ToString() + "?", "Complete",
                        "Cancel"))
                    {
                        await CompleteItem(kitepolLesson);
                    }
                }
            } 

            // prevents background getting highlighted
            KitepolLessonsList.SelectedItem = null;
        }

        // http://developer.xamarin.com/guides/cross-platform/xamarin-forms/working-with/listview/#context
        public async void OnComplete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var kitepolLesson = mi.CommandParameter as KitepolLesson;
            await CompleteItem(kitepolLesson);
        }

        // http://developer.xamarin.com/guides/cross-platform/xamarin-forms/working-with/listview/#pulltorefresh
        public async void OnRefresh(object sender, EventArgs e)
        {
            var list = (ListView)sender;
            Exception error = null;
            try
            {
                await RefreshItems(false, true);
            }
            catch (Exception ex)
            {
                error = ex;
            }
            finally
            {
                list.EndRefresh();
            }

            if (error != null)
            {
                await DisplayAlert("Refresh Error", "Couldn't refresh data (" + error.Message + ")", "OK");
            }
        }

        public async void OnSyncItems(object sender, EventArgs e)
        {
            await RefreshItems(true, true);
        }

        private async Task RefreshItems(bool showActivityIndicator, bool syncItems)
        {
            using (var scope = new KitepolWorkView.ActivityIndicatorScope(syncIndicator, showActivityIndicator))
            {
                var type = _kitePolWorkPageViewModel.SortLessonList[KitepolSortTypePicker.SelectedIndex];

                var allKitepolLessons = await _lessonManager.GetKitepolLessonItemsAsync("Wszystkie", syncItems);
                var kitepolLessons = await _lessonManager.GetKitepolLessonItemsAsync(type, syncItems);
                SumLessonsHours(allKitepolLessons);

                KitepolLessonsList.ItemsSource = kitepolLessons;
            }
        }

        private void SumLessonsHours(ObservableCollection<KitepolLesson> kitepolLessons)
        {
            float totalHours = kitepolLessons.Sum(kitepolLesson => kitepolLesson.Hours);
            float kiteHours = kitepolLessons.Where(kitepolLesson => kitepolLesson.Type == "Kitesurfing").Sum(kitepolLesson => kitepolLesson.Hours);
            float windHours = kitepolLessons.Where(kitepolLesson => kitepolLesson.Type == "Windsurfing").Sum(kitepolLesson => kitepolLesson.Hours);
            var allSalary = kiteHours * Constants.KitesurfingHourlyRate + windHours * Constants.WindsurfingHourlyRate;

            ThisYearKitepolHoursLabel.Text = "Godzinki w tym roku : " + totalHours.ToString() + "h" + " / " + kiteHours.ToString() + "h" + " / " + windHours.ToString() + "h";
            ThisYearKitepolSalary.Text = "Hajsik za ten sezon : " + allSalary + "zl";

        }

        // ukryj /pokaz panel dodawania a przy okazji wyzeruj dane wypisywane
        private void ShowOrHideNewLessonsPanel(object sender, EventArgs e)
        {
            int.TryParse(HowManyLessons.Text, out var lessonsAmount);
            _kitePolWorkPageViewModel.InitiatieAllLessons(lessonsAmount);

            NewLessonsListView.ItemsSource = _kitePolWorkPageViewModel.NewLessonsEntryList;

            // pokaz pola do dodania (Nazwa typ itd...)
            NewLessonStackLayout.IsVisible = !NewLessonStackLayout.IsVisible;
            SortKitepolPickerStackLayout.IsVisible = !SortKitepolPickerStackLayout.IsVisible;

            ExpandableAddPanelButton.Text = NewLessonStackLayout.IsVisible ? "-" : "+";
        }

        private void KitepolSortTypePicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            OnRefresh(KitepolLessonsList, e);
        }
    }
}
