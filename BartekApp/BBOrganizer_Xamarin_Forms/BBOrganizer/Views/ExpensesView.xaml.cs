﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Organizer1.Managers;
using Organizer1.Models;
using Organizer1.ViewModel;
using Plugin.LocalNotifications;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Organizer1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExpensesView : ContentPage
    {
        private readonly ExpensesManager _manager;

        private ExpensesPageViewModel _expensesPageViewModel;
        public bool ShowNewNoteGrid = false;


        public ExpensesView()
        {
            InitializeComponent();

            this.BindingContext = _expensesPageViewModel = new ExpensesPageViewModel();

            _manager = ExpensesManager.DefaultManager;

            ViewInitialization();

            // OnPlatform<T> doesn't currently support the "Windows" target platform, so we have this check here.
            if (_manager.IsOfflineEnabled &&
                (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.WinPhone))
            {
                var syncButton = new Button
                {
                    Text = "Sync items",
                    HeightRequest = 30
                };
                syncButton.Clicked += OnSyncItems;

                ButtonsPanel.Children.Add(syncButton);
            }
        }


        private class ActivityIndicatorScope : IDisposable
        {
            private bool _showIndicator;
            private ActivityIndicator _indicator;
            private Task _indicatorDelay;


            public ActivityIndicatorScope(ActivityIndicator indicator, bool showIndicator)
            {
                this._indicator = indicator;
                this._showIndicator = showIndicator;

                if (showIndicator)
                {
                    _indicatorDelay = Task.Delay(2000);
                    SetIndicatorActivity(true);
                }
                else
                {
                    _indicatorDelay = Task.FromResult(0);
                }
            }

            private void SetIndicatorActivity(bool isActive)
            {
                this._indicator.IsVisible = isActive;
                this._indicator.IsRunning = isActive;
            }

            public void Dispose()
            {
                if (_showIndicator)
                {
                    _indicatorDelay.ContinueWith(t => SetIndicatorActivity(false),
                        TaskScheduler.FromCurrentSynchronizationContext());
                }
            }
        }

        private void ViewInitialization()
        {
            SortTypePicker.SelectedIndex = 0;

            //SortTypePicker.SelectedIndexChanged += OnRefresh;
        }

        private async Task UpdateMonthExpenses()
        {
            var monthExpenses = await _manager.GetThisMonthExpenses();
            var amount = monthExpenses.Sum(monthExpense => monthExpense.Amount);

            MonthExpensesLabel.Text = "Wydatki w tym miesiacu : " + amount + " zl";
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            // Set syncItems to true in order to synchronize the data on startup when running in offline mode
            await RefreshItems(true, syncItems: true);
        }

        // Data methods
        async Task AddItem(Expense item)
        {
            await _manager.SaveTaskAsync(item);
            ExpensesList.ItemsSource = await _manager.GetExpenseItemsAsync("Wszystkie");
            await UpdateMonthExpenses();
        }

        async Task CompleteItem(Expense item)
        {
            item.Done = true;
            await _manager.SaveTaskAsync(item);
            ExpensesList.ItemsSource = await _manager.GetExpenseItemsAsync("Wszystkie");
            await UpdateMonthExpenses();
        }

        public async void OnAdd(object sender, EventArgs e)
        {
            var amount = 0;
            int.TryParse(NewItemExpenseAmount.Text, out amount);
            var expense = new Expense() { Name = NewItemName.Text, Type = _expensesPageViewModel.SelectedNoteType, Amount = amount , AddDateTime = DateTime.Now};
            await AddItem(expense);
            
            NewItemName.Unfocus();

            
            ShowOrHideNewNotePanel(null, null);
        }

        // Event handlers
        public async void OnSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var expense = e.SelectedItem as Expense;
            if (Device.OS != TargetPlatform.iOS && expense != null)
            {
                // Not iOS - the swipe-to-delete is discoverable there
                if (Device.OS == TargetPlatform.Android)
                {
                    await DisplayAlert(expense.Name, "Press-and-hold to complete task " + expense.Name, "Got it!");
                }
                else
                {
                    // Windows, not all platforms support the Context Actions yet
                    if (await DisplayAlert("Mark completed?", "Do you wish to complete " + expense.Name + "?", "Complete",
                        "Cancel"))
                    {
                        await CompleteItem(expense);
                    }
                }
            }

            // prevents background getting highlighted
            ExpensesList.SelectedItem = null;
        }

        // http://developer.xamarin.com/guides/cross-platform/xamarin-forms/working-with/listview/#context
        public async void OnComplete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var expense = mi.CommandParameter as Expense;
            await CompleteItem(expense);
        }

        // http://developer.xamarin.com/guides/cross-platform/xamarin-forms/working-with/listview/#pulltorefresh
        public async void OnRefresh(object sender, EventArgs e)
        {
            var list = (ListView)sender;
            Exception error = null;
            try
            {

                await UpdateMonthExpenses();
                await RefreshItems(false, true);
            }
            catch (Exception ex)
            {
                error = ex;
            }
            finally
            {
                list.EndRefresh();
            }

            if (error != null)
            {
                await DisplayAlert("Refresh Error", "Couldn't refresh data (" + error.Message + ")", "OK");
            }
        }

        public async void OnSyncItems(object sender, EventArgs e)
        {
            await RefreshItems(true, true);
        }

        private async Task RefreshItems(bool showActivityIndicator, bool syncItems)
        {
            using (var scope = new ExpensesView.ActivityIndicatorScope(syncIndicator, showActivityIndicator))
            {
                var type = _expensesPageViewModel.SortExpensesTypesList[SortTypePicker.SelectedIndex];

                var allExpensesList = await _manager.GetExpenseItemsAsync(type, syncItems);

                ExpensesList.ItemsSource = allExpensesList;
            }
        }

        // ukryj /pokaz panel dodawania a przy okazji wyzeruj dane wypisywane
        private void ShowOrHideNewNotePanel(object sender, EventArgs e)
        {
            //ukryj sortuj po....
            SortExpensesPickerPanel.IsVisible = !SortExpensesPickerPanel.IsVisible;

            // pokaz pola do dodania (Nazwa typ itd...)
            NewExpenseGrid.IsVisible = !NewExpenseGrid.IsVisible;

            ExpandableAddPanelButton.Text = NewExpenseGrid.IsVisible ? "-" : "+";

            //reset wpisywanych
            NewExpenseTypePicker.SelectedIndex = -1;
            NewItemName.Text = string.Empty;
        }

        private void SortTypePicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            OnRefresh(ExpensesList, e);
        }
    }
}
