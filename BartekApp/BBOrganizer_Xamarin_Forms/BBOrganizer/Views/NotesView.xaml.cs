﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MvvmHelpers;
using Organizer1.Models;
using Organizer1.ViewModel;
using Plugin.LocalNotifications;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Organizer1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotesView : ContentPage
    {
        private NoteManager _manager;

        private NotesPageViewModel _notesPageViewModel;
        public bool ShowNewNoteGrid = false;
        

        public NotesView()
        {
            InitializeComponent();

            this.BindingContext = _notesPageViewModel = new NotesPageViewModel();

            _manager = NoteManager.DefaultManager;

            ViewInitialization();

            // OnPlatform<T> doesn't currently support the "Windows" target platform, so we have this check here.
            if (_manager.IsOfflineEnabled &&
                (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.WinPhone))
            {
                var syncButton = new Button
                {
                    Text = "Sync items",
                    HeightRequest = 30
                };
                syncButton.Clicked += OnSyncItems;

                ButtonsPanel.Children.Add(syncButton);
            }
        }


        private class ActivityIndicatorScope : IDisposable
        {
            private bool _showIndicator;
            private ActivityIndicator _indicator;
            private Task _indicatorDelay;


            public ActivityIndicatorScope(ActivityIndicator indicator, bool showIndicator)
            {
                this._indicator = indicator;
                this._showIndicator = showIndicator;

                if (showIndicator)
                {
                    _indicatorDelay = Task.Delay(2000);
                    SetIndicatorActivity(true);
                }
                else
                {
                    _indicatorDelay = Task.FromResult(0);
                }
            }

            private void SetIndicatorActivity(bool isActive)
            {
                this._indicator.IsVisible = isActive;
                this._indicator.IsRunning = isActive;
            }

            public void Dispose()
            {
                if (_showIndicator)
                {
                    _indicatorDelay.ContinueWith(t => SetIndicatorActivity(false),
                        TaskScheduler.FromCurrentSynchronizationContext());
                }
            }
        }

        private void ViewInitialization()
        {
            SortTypePicker.SelectedIndex = 0;
            
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            // Set syncItems to true in order to synchronize the data on startup when running in offline mode
            await RefreshItems(true, syncItems: true);
        }

        // Data methods
        async Task AddItem(Note item)
        {
            await _manager.SaveTaskAsync(item);
            NotesList.ItemsSource = await _manager.GetNotesAsync("Wszystkie");
        }

        async Task CompleteItem(Note item)
        {
            item.Done = true;
            CrossLocalNotifications.Current.Cancel(item.NoteNotificationId);
            await _manager.SaveTaskAsync(item);
            NotesList.ItemsSource = await _manager.GetNotesAsync("Wszystkie");
        }

        public async void OnAdd(object sender, EventArgs e)
        {
            var random = new Random();
            var id = random.Next();
            var note = new Note {Name = NewItemName.Text, Type = _notesPageViewModel.SelectedNoteType, BodyText = NewItemBodyText.Text, NoteNotificationId = id};
            await AddItem(note);

           
            NewItemName.Unfocus();

            AddNotification(note, id);

            ShowOrHideNewNotePanel(null, null);
        }

        private void AddNotification(Note note, int noteNotificationId)
        {
            int days = 0, hours = 0, minutes = 0;
            int.TryParse(NewItemReminderDays.Text, out days);
            int.TryParse(NewItemReminderHours.Text, out hours);
            int.TryParse(NewItemReminderMinutes.Text, out minutes);

            if(days != 0 || hours != 0 || minutes != 0)
            {
                DateTime notificationDateTime = DateTime.Now.Add(new TimeSpan(days, hours, minutes, 0));
                NotificationController.SendScheduledNotification("Notatki",note.Name,noteNotificationId,notificationDateTime);
            }
        }

        // Event handlers
        public async void OnSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var todo = e.SelectedItem as Note;
            if (Device.OS != TargetPlatform.iOS && todo != null)
            {
                // Not iOS - the swipe-to-delete is discoverable there
                if (Device.OS == TargetPlatform.Android)
                {
                    await DisplayAlert(todo.Name, "Press-and-hold to complete task " + todo.Name, "Got it!");
                }
                else
                {
                    // Windows, not all platforms support the Context Actions yet
                    if (await DisplayAlert("Mark completed?", "Do you wish to complete " + todo.Name + "?", "Complete",
                        "Cancel"))
                    {
                        await CompleteItem(todo);
                    }
                }
            }

            // prevents background getting highlighted
            NotesList.SelectedItem = null;
        }

        // http://developer.xamarin.com/guides/cross-platform/xamarin-forms/working-with/listview/#context
        public async void OnComplete(object sender, EventArgs e)
        {
            var mi = ((MenuItem) sender);
            var todo = mi.CommandParameter as Note;
            await CompleteItem(todo);
        }

        // http://developer.xamarin.com/guides/cross-platform/xamarin-forms/working-with/listview/#pulltorefresh
        public async void OnRefresh(object sender, EventArgs e)
        {
            var list = (ListView) sender;
            Exception error = null;
            try
            {
                await RefreshItems(false, true);
            }
            catch (Exception ex)
            {
                error = ex;
            }
            finally
            {
                list.EndRefresh();
            }

            if (error != null)
            {
                await DisplayAlert("Refresh Error", "Couldn't refresh data (" + error.Message + ")", "OK");
            }
        }

        public async void OnSyncItems(object sender, EventArgs e)
        {
            await RefreshItems(true, true);
        }

        private async Task RefreshItems(bool showActivityIndicator, bool syncItems)
        {
            using (var scope = new ActivityIndicatorScope(syncIndicator, showActivityIndicator))
            {
                var type = _notesPageViewModel.SortNoteTypesList[SortTypePicker.SelectedIndex];

                var allNotesList = await _manager.GetNotesAsync(type, syncItems);


                NotesList.ItemsSource = allNotesList;
            }
        }

        // ukryj /pokaz panel dodawania a przy okazji wyzeruj dane wypisywane
        private void ShowOrHideNewNotePanel(object sender, EventArgs e)
        {
            //ukryj sortuj po....
            SortNotePickerPanel.IsVisible = !SortNotePickerPanel.IsVisible;

            // pokaz pola do dodania (Nazwa typ itd...)
            NewNoteGrid.IsVisible = !NewNoteGrid.IsVisible;

            ExpandableAddPanelButton.Text = NewNoteGrid.IsVisible ? "-" : "+";

            //reset wpisywanych
            NewNoteTypePicker.SelectedIndex = -1;
            NewItemName.Text = string.Empty;
            NewItemBodyText.Text = string.Empty;
            NewItemReminderDays.Text = string.Empty;
            NewItemReminderHours.Text = string.Empty;
            NewItemReminderMinutes.Text = string.Empty;
        }

        private void SortTypePicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            OnRefresh(NotesList, e);
        }
    }
}