﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBOrganizer;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Organizer1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : ContentPage
    {
        public MasterPage()
        {
            InitializeComponent();


        }
        

        private async void GoToNotes(object sender, EventArgs e)
        {
            await App.NavigateMasterDetail(new NotesView());
        }

        private async void GoToExpenses(object sender, EventArgs e)
        {
            await App.NavigateMasterDetail(new ExpensesView());
        }

        private async void GoToKitepol(object sender, EventArgs e)
        {
            await App.NavigateMasterDetail(new KitepolWorkView());
        }
    }
}
