﻿using System;
using System.Threading.Tasks;
using Organizer1.Views;
using Xamarin.Forms;

namespace BBOrganizer
{
    public class App : Application
    {
        public static MasterDetailPage MasterDetailPage { get; set; }

        public static async Task NavigateMasterDetail(Page page)
        {
            App.MasterDetailPage.IsPresented = false;
            await App.MasterDetailPage.Detail.Navigation.PushAsync(page);
        }

        public App()
        {
            // The root page of your application
            MainPage = new Organizer1.Views.Menu();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}

